
const Course = require("../models/product.js");
const User = require("../models/user.js");

// add product
module.exports.addProduct = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return false
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            return newCourse.save().then((course, error) => {
      
                if(error) {
                    return false
                } else {
                    return true
                }
            })
        }
        
    })
};

// get active products
module.exports.getActiveProducts = () => {
	return Course.find({}).then (result => {
		return result;
	})
};

// update product
module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true) {
		return Course.findByIdAndUpdate(productId, 
			{	
				name: newData.product.name,
				description: newData.product.description,
				price: newData.product.price
			}
		).then((updatedProduct, error) => {
			if(error){
				return false
			}
			return {
				message : "Product updated successfully"
			}
		})

		}
		else{
			let message = Promise.resolve('User must be ADMIN to access this!')
			return message.then((value) => {return value})
	}
};


// GET specficic product
module.exports.getProduct = (productId) => {
						
	return Course.findById(productId).then(result => {
		return result;
	})
};


// archived product
module.exports.archiveProduct = (productId) => {
	return Course.findByIdAndUpdate(productId, {
		isActive: false
	})

	.then((archivedProduct, error) => {
		if(error) {
			return false
		}

		return{
			message: "Product archived successfully"
		}
	})
};

