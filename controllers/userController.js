// require model so we could use the model for searching
const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../models/product.js");

module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName:reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 0),
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

};

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			// compareSync is bcrypt function to compare a unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return false;
			}
		}
	})
};

module.exports.userDetails = (userInfo) => {
	return User.findById(userInfo._id).then((details, err) => {
		if(err){
			return false;
		}
		else{
			details.password = "*****";
			return details;
		}
	})
};


module.exports.checkout = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.items.push({productId: data.productId});

		return user.save().then((user, error) => {
			if(error) {
				return false;
			} else{
				return true;
			}
		})

	})

	let isProductUpdated = await Course.findById(data.productId).then(product => {

		product.purchaser.push({userId : data.userId});

		return product.save().then((product, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	})

	if(isUserUpdated && isProductUpdated) {
		return true;
	} else {
		return false;
	}

};



