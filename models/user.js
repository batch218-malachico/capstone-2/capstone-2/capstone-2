const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
		firstName: {
			type : String,
			required : [true, "FirstName is required"]
		},

		lastName: {
			type: String,
			required: [true, "LastName is required"]
		},

		email: {
			type : String,
			required : [true, "Email is required"]
		},

		password:{
			type: String,
			required : [true, "Password is required"]
		},

		isAdmin :{
			type : Boolean,
			default: false
		},

		mobileNo :{
			type: String,
			required : [true, "Mobile No. is required"]

		},

		items : [
				{
					productId : {
					type: String,
					required : [true, "ProductId is required"]
					},

					purchasedOn : {
						type: Date,
						default: new Date()
					},

					status: {
						type: String,
						default: "Order was successfully created"
					}
				}

		]
});

module.exports = mongoose.model("User", userSchema);
