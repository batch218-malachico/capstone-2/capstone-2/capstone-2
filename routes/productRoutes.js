
const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");


// add product
router.post("/add", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    productController.addProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
});


// get all active products
router.get("/active", (req, res) =>{
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
});


// update product
router.patch("/:productId/update", auth.verify, (req, res) =>
{
	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(req.params.productId, newData).then(
		resultFromController => {
		res.send(resultFromController)
	});
});


// get specific specific product
router.get("/:productId", (req, res) => {

	productController.getProduct(req.params.productId).then(
		resultFromController => res.send(resultFromController))
});

// archive product
router.put("/:productId/archive", auth.verify, (req,res) => {
	productController.archiveProduct(req.params.productId).then(resultFromController => {
		res.send(resultFromController)
	});
});


module.exports = router;